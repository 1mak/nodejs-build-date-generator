# NodeJS Build Date Generator

A little helper function to write the current date and time into an .env file so that we can use that in the build process.


1.  Add this to the package.json: `npm run generate-build-date`
2.  Create an .env-sample file with `%BUILD_DATE%` as placeholder
3.  Run `npm run generate-build-number`
