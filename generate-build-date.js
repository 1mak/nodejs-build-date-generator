const fs = require('fs')
const srcFilename = '.env-sample'
const targetFilename = '.env'
const dateObject = new Date()

fs.readFile(srcFilename, 'utf8', function (err,data) {
  if (err) {
    return console.log(err)
  }
  const result = data.replace(/%BUILD_DATE%/g, dateObject)
  
  fs.writeFile(targetFilename, result, 'utf8', function (err) {
    if (err) return console.log(err)
  })
})
